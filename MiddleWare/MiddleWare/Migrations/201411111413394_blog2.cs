namespace MiddleWare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class blog2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.TagID);
            
            AddColumn("dbo.Posts", "DateUpdate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Posts", "Tag_TagID", c => c.Int());
            AddColumn("dbo.Comments", "DateUpdate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Comments", "Author", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Posts", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false));
            AddForeignKey("dbo.Posts", "Tag_TagID", "dbo.Tags", "TagID");
            CreateIndex("dbo.Posts", "Tag_TagID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts", new[] { "Tag_TagID" });
            DropForeignKey("dbo.Posts", "Tag_TagID", "dbo.Tags");
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.Posts", "Body", c => c.String());
            AlterColumn("dbo.Posts", "Title", c => c.String());
            DropColumn("dbo.Comments", "Author");
            DropColumn("dbo.Comments", "DateUpdate");
            DropColumn("dbo.Posts", "Tag_TagID");
            DropColumn("dbo.Posts", "DateUpdate");
            DropTable("dbo.Tags");
        }
    }
}
