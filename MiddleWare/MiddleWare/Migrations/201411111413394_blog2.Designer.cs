// <auto-generated />
namespace MiddleWare.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class blog2 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(blog2));
        
        string IMigrationMetadata.Id
        {
            get { return "201411111413394_blog2"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
