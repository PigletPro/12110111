namespace MiddleWare.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class blog3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Posts", "Tag_TagID", "dbo.Tags");
            DropIndex("dbo.Posts", new[] { "Tag_TagID" });
            CreateTable(
                "dbo.TagPosts",
                c => new
                    {
                        Tag_TagID = c.Int(nullable: false),
                        Post_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_TagID, t.Post_Id })
                .ForeignKey("dbo.Tags", t => t.Tag_TagID, cascadeDelete: true)
                .ForeignKey("dbo.Posts", t => t.Post_Id, cascadeDelete: true)
                .Index(t => t.Tag_TagID)
                .Index(t => t.Post_Id);
            
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false));
            DropColumn("dbo.Posts", "DateUpdate");
            DropColumn("dbo.Posts", "Tag_TagID");
            DropColumn("dbo.Comments", "DateUpdate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comments", "DateUpdate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Posts", "Tag_TagID", c => c.Int());
            AddColumn("dbo.Posts", "DateUpdate", c => c.DateTime(nullable: false));
            DropIndex("dbo.TagPosts", new[] { "Post_Id" });
            DropIndex("dbo.TagPosts", new[] { "Tag_TagID" });
            DropForeignKey("dbo.TagPosts", "Post_Id", "dbo.Posts");
            DropForeignKey("dbo.TagPosts", "Tag_TagID", "dbo.Tags");
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false, maxLength: 500));
            DropTable("dbo.TagPosts");
            CreateIndex("dbo.Posts", "Tag_TagID");
            AddForeignKey("dbo.Posts", "Tag_TagID", "dbo.Tags", "TagID");
        }
    }
}
