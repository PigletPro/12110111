﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MiddleWare.Models
{
    public class Tag
    {
        public int TagID { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "So ky tu trong khoang tu 10 - 100", MinimumLength = 10)]
        public string Content { get; set; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}