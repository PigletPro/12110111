namespace Blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.String(nullable: false, maxLength: 128),
                        Email = c.String(),
                        Password = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.AccountID);
            
            AddColumn("dbo.BaiViet", "AccountID", c => c.Int(nullable: false));
            AddColumn("dbo.BaiViet", "Account_AccountID", c => c.String(maxLength: 128));
            AlterColumn("dbo.BaiViet", "ID", c => c.Int(nullable: false));
            AlterColumn("dbo.BaiViet", "Title", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Comments", "Body", c => c.String(maxLength: 250));
            AlterColumn("dbo.Comments", "Author", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Tags", "Content", c => c.String(maxLength: 100));
            AddForeignKey("dbo.BaiViet", "Account_AccountID", "dbo.Accounts", "AccountID");
            CreateIndex("dbo.BaiViet", "Account_AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.BaiViet", new[] { "Account_AccountID" });
            DropForeignKey("dbo.BaiViet", "Account_AccountID", "dbo.Accounts");
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.Comments", "Author", c => c.String());
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.BaiViet", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.BaiViet", "ID", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.BaiViet", "Account_AccountID");
            DropColumn("dbo.BaiViet", "AccountID");
            DropTable("dbo.Accounts");
        }
    }
}
