namespace Blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan6 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Accounts", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "FirstName", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Accounts", "LastName", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "LastName", c => c.String());
            AlterColumn("dbo.Accounts", "FirstName", c => c.String());
            AlterColumn("dbo.Accounts", "Password", c => c.String());
            AlterColumn("dbo.Accounts", "Email", c => c.String());
        }
    }
}
