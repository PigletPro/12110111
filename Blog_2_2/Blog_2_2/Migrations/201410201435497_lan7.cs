namespace Blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan7 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViet", "Body", c => c.String(nullable: false, maxLength: 250));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false, maxLength: 250));
            AlterColumn("dbo.Accounts", "LastName", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "LastName", c => c.String(maxLength: 50));
            AlterColumn("dbo.Comments", "Body", c => c.String(maxLength: 250));
            AlterColumn("dbo.BaiViet", "Body", c => c.String(maxLength: 250));
        }
    }
}
