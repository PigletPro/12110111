﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Tag
    {
        public int TagID { get; set; }

        [StringLength(100,ErrorMessage="So ky tu trong khoang tu 10 - 100",MinimumLength=10)]
        public string Content { get; set; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}