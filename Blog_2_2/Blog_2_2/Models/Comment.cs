﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Comment
    {
        public int ID { get; set; }
        
        [Required]
        [StringLength(250, ErrorMessage = "So luong ky tu trong khoang 50 - 250", MinimumLength = 50)]
        public string Body { get; set; }
        
        [DataType(DataType.Date)]
        public System.DateTime DateCreated { get; set; }

        [DataType(DataType.Date)]
        public System.DateTime DateUpdate { get; set; }

        [Required]
        [StringLength(100,ErrorMessage="Ban chi duoc nhap toi da 100 ky tu")]
        public string Author { get; set; }

        
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}