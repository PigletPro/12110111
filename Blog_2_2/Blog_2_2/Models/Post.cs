﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    [Table("BaiViet")]
    public class Post
    {
        [Range(2,100)]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int ID { set; get; }
        [Required]
        [StringLength(100,ErrorMessage="So luong ky tu trong khoang 10 - 100",MinimumLength=10)]
        public String Title { set; get; }
        [Required]
        [StringLength(250,ErrorMessage="So luong ky tu trong khoang 50 - 250",MinimumLength=50)]
        public String Body { set; get; }

        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }

        [DataType(DataType.Date)]
        public DateTime DateUpdate { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }

        public int AccountID {set; get;}
        public virtual Account Account {set; get;}
    }
}