﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Account
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }
        [Required]
        [DataType(DataType.Password)]
        public String Password { set; get; }
        [Required]
        [StringLength(50,ErrorMessage="Ban chi nhap toi da 50 ky tu")]
        public String FirstName { set; get; }
        [Required]
        [StringLength(50, ErrorMessage = "Ban chi nhap toi da 50 ky tu")]
        public String LastName { set; get; }
        public String AccountID { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}