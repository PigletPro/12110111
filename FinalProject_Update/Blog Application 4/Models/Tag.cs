﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_Application_4.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "So ky tu trong khoang tu 10 - 100", MinimumLength = 10)]
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}