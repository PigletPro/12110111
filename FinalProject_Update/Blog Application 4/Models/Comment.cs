﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_Application_4.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required]
        
        public String Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}