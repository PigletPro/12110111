// <auto-generated />
namespace Blog_Application_4.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class tag2 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(tag2));
        
        string IMigrationMetadata.Id
        {
            get { return "201411201552134_tag2"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
