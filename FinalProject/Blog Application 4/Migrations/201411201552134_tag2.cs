namespace Blog_Application_4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tag2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "Title", c => c.String());
            DropColumn("dbo.Posts", "Tille");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Posts", "Tille", c => c.String());
            DropColumn("dbo.Posts", "Title");
        }
    }
}
