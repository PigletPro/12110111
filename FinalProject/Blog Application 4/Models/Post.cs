﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_Application_4.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "So luong ky tu trong khoang 20- 500", MinimumLength = 20)]
        public String Title { set; get; }
        [Required]
        [MinLength(50,ErrorMessage="So luong ky tu toi thieu la 50")]
        public String Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}